package
{
	import org.flixel.*;
	import flash.utils.getTimer;
	
	public class Player extends FlxSprite
	{
		[Embed(source = "data/guy.png")] private var ImgPlayer:Class;
		[Embed(source = "data/guyrun.png")] private var ImgRun:Class;
		[Embed(source = "data/guyjump.png")] private var ImgJump:Class;
		
		public var guyIdle:FlxSprite;
		private var lastFired:int = 0;
		private var bulletDelay:int = 300;
		private var currentlyVisible:FlxSprite;
		
		public function Player(X:Number, Y:Number)
		{
			super(X, Y);
			loadAllAssets(X, Y);
			maxVelocity.x = 100;			//walking speed
			acceleration.y = 400;			//gravity
			drag.x = maxVelocity.x*4;		//deceleration (sliding to a stop)
			
			//tweak the bounding box for better feel
			width = 3;
			height = 36;
			offset.x = 0;
			offset.y = 3;
			
			
			loadGraphic(ImgRun, true, true, 44, 36);
			addAnimation("walk", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 9, 4, 5, 6], 14, true);
			
			currentlyVisible = this;
		}
		
		private function loadAllAssets(X:Number, Y:Number):void
		{
			guyIdle = new FlxSprite(X, Y);
			guyIdle.loadGraphic(ImgPlayer, true, true, 35, 39);
			guyIdle.visible = false;
			guyIdle.width = 14;
			guyIdle.height = 39;
			guyIdle.offset.x = -4;
			guyIdle.offset.y = 3;
			guyIdle.addAnimation("idle", [0, 1, 2, 3, 4, 5, 6, 7, 8], 12, false);
			Registry.guyIdle = guyIdle;
			
			Registry.guyJump = new FlxSprite(X, Y);
			Registry.guyJump.loadGraphic(ImgJump, true, true, 42, 54);
			Registry.guyJump.visible = false;
			Registry.guyJump.width = 20;
			Registry.guyJump.height = 39;
			Registry.guyJump.offset.x = 3;
			Registry.guyJump.offset.y = 3;
			Registry.guyJump.addAnimation("jump", [0, 1, 2, 3], 12, false);
		}
		
		private function updateAssets():void
		{
			Registry.guyIdle.x = this.x;
			Registry.guyIdle.y = this.y;
			Registry.guyIdle.facing = this.facing;
			Registry.guyJump.x = this.x;
			Registry.guyJump.y = this.y;
			Registry.guyJump.facing = this.facing;
		}
		
		override public function update():void
		{
			//gun.x = this.x + 10;
			//gun.y = this.y - 10;
			//Smooth slidey walking controls
			acceleration.x = 0;
			if(FlxG.keys.LEFT)
			{
				facing = FlxObject.LEFT;
				acceleration.x -= drag.x;
			}
			else if(FlxG.keys.RIGHT)
			{
				facing = FlxObject.RIGHT;
				acceleration.x += drag.x;
			}
				
			if(isTouching(FLOOR))
			{
				//Jump controls
				if(FlxG.keys.justPressed("SPACE"))
				{
					
					velocity.y = -acceleration.y * 0.8;
					clearVisibility();
					Registry.guyJump.visible = true;
					Registry.guyJump.play("jump");
					currentlyVisible = Registry.guyJump;
				}//Animations
				else if (velocity.x > 0)
				{
					clearVisibility();
					this.visible = true;
					play("walk");
					currentlyVisible = this;
				}
				else if (velocity.x < 0)
				{
					clearVisibility();
					this.visible = true;
					play("walk");
					currentlyVisible = this;
				}
				else
				{
					clearVisibility();
					Registry.guyIdle.visible = true;
					Registry.guyIdle.play("idle");
					currentlyVisible = Registry.guyIdle;
				}
					
			}
			else if (facing == FlxObject.RIGHT)
				play("idle_right");
			else if (facing == FlxObject.LEFT)
				play("idle_left");
			
			if (FlxG.keys.X && getTimer() > lastFired + bulletDelay)
			{
				Registry.bullets.fire(x + currentlyVisible.width/2, y + currentlyVisible.height/2, facing == FlxObject.RIGHT ? 1 : -1);
				lastFired = getTimer();
			}
			
			updateAssets();
			//Default object physics update
			super.update();
		}
		
		private function clearVisibility():void
		{
			Registry.guyJump.visible = false;
			Registry.guyIdle.visible = false;
			this.visible = false;
		}
		
	}
	
}