package  
{
	import org.flixel.*;
	import flash.utils.getTimer;

	public class EnemyManager extends FlxGroup
	{
		private var lastReleased:int;
		//private var releaseRate:int = 500;
		private var hitMarkers:Array;
		private var tx:FlxText;
		public function EnemyManager() 
		{
			super();
			hitMarkers = new Array();
			//for (var i:int = 0; i < 100; i++)
			//{
				var enemy:Enemy = new Enemy();
				add(enemy.hp);
				add(enemy);
				release();
			//}
		}
		
		public function release():void
		{
			var enemy:Enemy = Enemy(getFirstAvailable());
			
			if (enemy)
			{
				enemy.launch();
			}
		}
		
		override public function update():void
		{
			super.update();
			for each (var f:FlxText in hitMarkers)
			{
				f.y--;
				f.alpha -= .025;
				if (f.alpha <= 0)
				{
					
					f.kill();
				}
			}
			//if (getTimer() > lastReleased + releaseRate)
			//{
				//lastReleased = getTimer();
				
				//release();
			//}
		}
		
		public function bulletHitEnemy(bullet:Bullet, enemy:Enemy):void
		{
			
			
			enemy.hurt(bullet.damage);
			trace(bullet.damage);
			tx = new FlxText(enemy.hp.x, enemy.hp.y + 3, FlxG.width, "" + bullet.damage);
			tx.color = 0x181dee;
			hitMarkers.push(tx);
			add(tx);
			bullet.kill();
			FlxG.score += 1;
		}
		
	}

}