package  
{
        import flash.display.Stage;
        import org.flixel.*;
        
        public class Registry 
        {
                public static var stage:Stage;
				public static var state:FlxState;
                
                public static var player:Player;
				public static var guyIdle:FlxSprite;
				public static var guyJump:FlxSprite;
				public static var cameraFocuses:Array;
				public static var customCamera:CustomCamera;
				public static var bullets:BulletManager = new BulletManager();
				public static var map:FlxTilemap;
				public static var enemies:EnemyManager = new EnemyManager();
                /*public static var enemies:EnemyManager;
                public static var inventory:Inventory;
                public static var map:Map;
                public static var weapons:Weapons;
                public static var potions:Potions;
                public static var spells:Spells;
                
                public static var previousLevel:int;
                public static var enemiesKilledThisLevel:int;
                public static var enemiesKilledThisGame:int;
                public static var arrowsFiredThisGame:int;*/
                
                public function Registry() 
                {
                }
                
        }
}