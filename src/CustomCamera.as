package  
{
	import org.flixel.FlxCamera;
	/**
	 * ...
	 * @author Hunter Martin
	 */
	public class CustomCamera extends FlxCamera
	{
		
		public function CustomCamera(X:int,Y:int,Width:int,Height:int,Zoom:Number=0) 
		{
			super(X, Y, Width, Height, Zoom);
		}
		
		override public function update():void
		{
			super.update();
		}
		
	}

}