package  
{
	import org.flixel.*;

	public class Enemy extends FlxSprite
	{
		[Embed(source = 'data/guy.png')] private var enemyPNG:Class;
		[Embed(source = 'data/health.png')] private var healthBar:Class;
		public var hp:FlxSprite;
		private var fullHealth:int = 100;
		public function Enemy() 
		{
			super(0, 0);
			hp = new FlxSprite(0, 0, healthBar);
			loadGraphic(enemyPNG, true, true, 35, 39);
			visible = true;
			width = 33;
			height = 36;
			offset.x = 5;
			offset.y = 3;
			facing = FlxObject.RIGHT;
			addAnimation("idle", [0, 1, 2, 3, 4, 5, 6, 7, 8], 12, false);
			hp.scale.x = 20;
			hp.scale.y = 3;
			hp.visible = false;
			exists = false;
		}
		
		public function launch():void
		{
			x = 150;
			y = 200;
			acceleration.y = 400;
			hp.x = 150;
			hp.y = 190;
			health = fullHealth;
			exists = true;
			hp.visible = true;
		}
		
		override public function kill():void
		{
			super.kill();
			hp.kill();
			FlxG.score += 20;
		}
		
		override public function update():void
		{
			super.update();
			play("idle");
			hp.x = this.x + this.width/2 - hp.width;
			hp.y = this.y - 10;
			hp.scale.x = health / fullHealth * 20;
			if (false)
			{
				exists = false;
				hp.visible = false;
			}
		}
		
	}

}