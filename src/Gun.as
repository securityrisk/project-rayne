package  
{
	import org.flixel.*;
	/**
	 * ...
	 * @author Hunter Martin
	 */
	
	public class Gun extends FlxSprite
	{
		[Embed(source = "data/gun01.png")] private var ImgGun:Class;
		public function Gun(X:Number, Y:Number) 
		{
			super(X+ 10, Y-10);
			loadGraphic(ImgGun, true, true, 39, 23);
			maxVelocity.x = 100;			//walking speed
			acceleration.y = 400;			//gravity
			drag.x = maxVelocity.x*4;		//deceleration (sliding to a stop)
			//width = 40;
			//height = 12;
			offset.x = 3;
			offset.y = 3;
			
			addAnimation("shooting", [0,1], 12, false);
			addAnimation("idle", [1], 0, false);
			trace("Working Gun", x, y);
		}
		
		override public function update():void
		{
			if(FlxG.keys.LEFT)
			{
				facing = FlxObject.LEFT;
			}
			else if(FlxG.keys.RIGHT)
			{
				facing = FlxObject.RIGHT;
			}
			if(FlxG.keys.X)
			{
				play("shooting");
				trace("SHOOTING");
			}
			else
				play("idle");
			
			super.update();
		}
		
	}

}