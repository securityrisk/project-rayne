package
{
	import flash.events.Event;
	import flash.display.BlendMode;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import XML;
	
	import net.pixelpracht.tmx.TmxMap;
	import net.pixelpracht.tmx.TmxObject;
	import net.pixelpracht.tmx.TmxObjectGroup;
	
	import org.flixel.*;
	
	public class PlayStateTmx extends FlxState
	{
		[Embed(source='alt_tiles.png')] private static var ImgTiles:Class;
		[Embed(source="data/bg.png")] private static var ImgBG:Class;
		
		protected var _player:Player = new Player(0,0);
		protected var tx:FlxText;
		protected var loader:URLLoader;
		protected var request:URLRequest;
		
		override public function create():void
		{
			add(Registry.bullets);
			add(Registry.enemies)
			tx = new FlxText(2,FlxG.height-12,FlxG.width,"Interact with ARROWS + SPACE. Shoot with X");
			tx.scrollFactor.x = tx.scrollFactor.y = 0;
			tx.color = 0xFFFFFF;
			tx.shadow = 0x233e58;
			
			Registry.state = this;
			Registry.player = _player;
			Registry.cameraFocuses = new Array();
			Registry.customCamera = new CustomCamera(FlxG.camera.x, FlxG.camera.y, FlxG.camera.width, FlxG.camera.height, FlxG.camera.zoom);
			Registry.customCamera.active = true;
			Registry.customCamera.visible = true;
			
			FlxG.addCamera(Registry.customCamera);
			
			loadTmxFile();
		}
		
		override public function update():void
		{
			super.update();
			if (Registry.cameraFocuses.length > 0 && Math.abs(Registry.player.x - Registry.cameraFocuses[0].x) < 220)
			{
				FlxG.camera.follow(null);
				var mid:FlxPoint = new FlxPoint(Math.abs(Registry.player.x + Registry.cameraFocuses[0].x) / 2, Math.abs(Registry.player.y + Registry.cameraFocuses[0].y) / 2);
				FlxG.camera.focusOn(mid);
			}
			else
			{
				if (Registry.cameraFocuses.length > 0)
				{
					//var mid:FlxPoint = new FlxPoint(Math.abs(Registry.player.x + Registry.cameraFocuses[0].x) / 2, Math.abs(Registry.player.y + Registry.cameraFocuses[0].y) / 2);
				}
				FlxG.camera.follow(Registry.player);
			}
			FlxG.overlap(Registry.bullets, Registry.enemies, Registry.enemies.bulletHitEnemy);
			FlxG.collide(Registry.player, Registry.map);
			FlxG.collide(Registry.bullets, Registry.map);
			FlxG.collide(Registry.enemies, Registry.map);
		}
		
		private function loadTmxFile():void
		{
			loader = new URLLoader(); 
			loader.addEventListener(Event.COMPLETE, onTmxLoaded);
			request = new URLRequest("map02.tmx");
			loader.load(request);
		}
		
		private function onTmxLoaded(e:Event):void
		{
			var xml:XML = new XML(e.target.data);
			var tmx:TmxMap = new TmxMap(xml);
			loadStateFromTmx(tmx);
		}
		
		private function loadStateFromTmx(tmx:TmxMap):void
		{			
			//Background
			FlxG.bgColor = 0xffacbcd7;
			var decoration:FlxSprite = new FlxSprite(256,159,ImgBG);
			decoration.moves = false;
			decoration.solid = false;
			add(decoration);
			
			//create the flixel implementation of the objects specified in the ObjectGroup 'objects'
			var group:TmxObjectGroup = tmx.getObjectGroup('objects');
			for each(var object:TmxObject in group.objects)
				spawnObject(object)
			
			//Basic level structure
			var t:FlxTilemap = new FlxTilemap();
			//generate a CSV from the layer 'map' with all the tiles from the TileSet 'tiles'
			var mapCsv:String = tmx.getLayer('map').toCsv(tmx.getTileSet('tiles'));
			t.loadMap(mapCsv,ImgTiles);
			t.follow(Registry.customCamera);
			Registry.map = t;
			add(t);
			add(tx);
		}
		
		
		private function spawnObject(obj:TmxObject):void
		{
			//Add game objects based on the 'type' property
			switch(obj.type)
			{
				case "player":
					Registry.player = new Player(obj.x, obj.y);
					add(Registry.player);
					add(Registry.guyIdle);
					add(Registry.guyJump);
					Registry.customCamera.follow(Registry.player);
					FlxG.camera = Registry.customCamera;
					FlxG.cameras.push(Registry.customCamera);
					FlxG.camera.follow(Registry.player);
					FlxG.cameras.shift();
					trace(FlxG.cameras.indexOf(FlxG.camera));
					return;
				case "cameraFocus":
					Registry.cameraFocuses.push(new FlxPoint(obj.x, obj.y));
					return;
			}
		}
		
		

	}
}