package  
{
    import org.flixel.*;
 
    public class Bullet extends FlxSprite
    {
        [Embed(source = 'data/bullet.png')] private var bulletPNG:Class;
        
        public var damage:int;
        public var speed:int = 300;
        
        public function Bullet() 
        {
            super(0, 0, bulletPNG);
            
            //    We do this so it's ready for pool allocation straight away
            exists = false;
        }
 
        public function fire(bx:int, by:int, facing:int):void
        {
			damage = (int)(Math.random() * 4) + 10;
            x = bx;
            y = by;
            velocity.x = facing*speed;
            exists = true;
        }
        
        override public function update():void
        {
            super.update();
            
            //    Bullet off the top of the screen?
            if (exists && (x > width + Registry.map.width || x < 0) || Math.abs(velocity.x) == 0)
            {
                exists = false;
            }
        }
        
    }
 
}